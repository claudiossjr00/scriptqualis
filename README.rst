===============================
scriptqualis
===============================

.. image:: https://app.wercker.com/status/f0838d931d919d7097924d8dbe10ab75/s
        :target: https://app.wercker.com/project/bykey/f0838d931d919d7097924d8dbe10ab75

.. image:: https://img.shields.io/pypi/v/scriptqualis.svg
        :target: https://pypi.python.org/pypi/scriptqualis

Ferramenta para extrair informações do Qualis CAPES e as salvar localmente. Os arquivos locais podem então ser usados como entrada para o #scriptLattes.

* Free software: BSD license
* Documentation: https://scriptqualis.readthedocs.org.

Features
--------

* TODO
